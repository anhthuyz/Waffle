package io.thuyca.waffle.testng;

import java.util.Arrays;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

public class TestNGPilot {
	public static synchronized TestNGPilot getPilot() {
		if (pilot == null) {
			pilot = new TestNGPilot();
		}
		return pilot;
	}

	public void runTestSuite(String suiteFileName) {

		try {
			testng = new TestNG();
			testng.setPreserveOrder(true);
			testng.setVerbose(2);

			// create a suite programmatically
			XmlSuite suite = new XmlSuite();

			suite.addListener("com.fpt.waffle.testng.SuiteListener");
			suite.addListener("com.fpt.waffle.testng.TestListener");
			suite.addListener("com.fpt.waffle.testng.MethodListener");

			// add a suite-file to the suite
			suite.setSuiteFiles(Arrays.asList(suiteFileName));

			// 2. to run with XmlSuite, uncomment this one, comment 1
			testng.setXmlSuites(Arrays.asList(suite));

			testng.run();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	};

	private TestNG testng;

	public TestNGPilot() {
	}

	private static TestNGPilot pilot;
}
