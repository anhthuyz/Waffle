package io.thuyca.waffle.testng;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;

import io.thuyca.waffle.selenium.Browser;

public class TestListener implements ITestListener {
	@Override
	public void onTestStart(ITestResult result) {

		log.info("Test started: " + result.getMethod().getMethodName());
	}

	@Override
	public void onStart(ITestContext context) {
		try {
			Reporter.log("Open browser to start testing");
			Browser.getBrowser().launch();
		} catch (Exception e) {
			throw new RuntimeException("Can't launch the suite.", e);
		}
	}

	@Override
	public void onFinish(ITestContext context) {
		Reporter.log("Shutting down the browser.");
		Browser.getBrowser().shutdown();
	}

	private final Log log = LogFactory.getLog(TestListener.class);

	@Override
	public void onTestSuccess(ITestResult result) {
		ITestNGMethod method = result.getMethod();
		log.info(method.getMethodName() + " PASSED");
	}

	@Override
	public void onTestFailure(ITestResult result) {
		ITestNGMethod method = result.getMethod();
		log.info(method.getMethodName() + " FAILED");
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		ITestNGMethod method = result.getMethod();
		log.info(method.getMethodName() + " SKIPPED");
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		ITestNGMethod method = result.getMethod();
		log.info(method.getMethodName() + " Failed (within success margin)");
	}

}
