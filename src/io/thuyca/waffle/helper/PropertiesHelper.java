package io.thuyca.waffle.helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesHelper {
	public static synchronized PropertiesHelper getProperties() {
		if (properties == null) {
			properties = new PropertiesHelper();
		}
		return properties;
	}

	public void setProperty(String property, String value) {
		FileInputStream in = null;
		try {
			in = new FileInputStream(PROPERTIES_FILE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Properties props = new Properties();
		try {
			props.load(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		FileOutputStream out = null;
		try {
			out = new FileOutputStream(PROPERTIES_FILE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		props.setProperty(property, value);
		try {
			props.store(out, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getProperty(String property) {
		Properties prop = new Properties();
		InputStream input = null;
		
		String retVal = "";
		try {

			input = new FileInputStream(PROPERTIES_FILE);

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			retVal = prop.getProperty(property);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return retVal;
	}

	public PropertiesHelper() {
	};

	private static PropertiesHelper properties;
	private static final String PROPERTIES_FILE = "atu.properties";
}
