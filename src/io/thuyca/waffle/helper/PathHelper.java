package io.thuyca.waffle.helper;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import io.thuyca.waffle.helper.PlatformHelper;

public class PathHelper {
	public static synchronized PathHelper getPath() {
		if (pathProvider == null) {
			pathProvider = new PathHelper();
		}
		return pathProvider;
	}

	private String getTestPackageQualifiedName(String configName) {
		return PACKAGE_NAME_PREFIX + configName + PACKAGE_NAME_SUFFIX;
	}

	public String getRelativePathOfTestPackage(String configName) {
		String retVal = "";
		if (PlatformHelper.getPlatform().isWindows()) {
			retVal = "bin\\"
					+ getTestPackageQualifiedName(configName)
							.replace(".", "\\");
		} else {
			retVal = "bin/"
					+ getTestPackageQualifiedName(configName).replace(".", "/");
		}
		return retVal;
	}

	public List<String> getTestClassesInsidePackage(String configName) {
		String path = getRelativePathOfTestPackage(configName);
		List<String> retList = new ArrayList<String>();

		File folder = new File(path);
		File[] listOfFiles = folder.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".class");
			}
		});
		for (File file : listOfFiles) {
			retList.add(file.getName().replace(".class", ""));
		}

		return retList;
	}

	public PathHelper() {
	};

	private static PathHelper pathProvider;

	private static final String PACKAGE_NAME_PREFIX = "com.fpt.waffle.";
	private static final String PACKAGE_NAME_SUFFIX = ".test";
}
