package io.thuyca.waffle.helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelHelper {
	
	public ExcelHelper(String excel_file_name){
		FileInputStream fis;
		try {
			fis = new FileInputStream(excel_file_name);
			wb = new XSSFWorkbook(fis);
		} catch (FileNotFoundException e1) {
			System.out.println("Excel file not found:"+excel_file_name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public ExcelHelper(){};
	
	//This method must be call first if ExcelHelper is initialized by using empty constructor
	public void parseFile(String excel_file_name){
		FileInputStream fis;
		try {
			fis = new FileInputStream(excel_file_name);
			wb = new XSSFWorkbook(fis);
		} catch (FileNotFoundException e1) {
			System.out.println("Excel file not found:"+excel_file_name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	// Get a cell value and pass into a string
	public String getCellValueAsString(String sheet_name, String cell_reference){
		String ret = "";
		sheet = wb.getSheet(sheet_name);
		CellReference cellReference = new CellReference(cell_reference);
		Row row = sheet.getRow(cellReference.getRow());
		Cell cell = row.getCell(cellReference.getCol()); 
		
		if (cell!=null) {
		    switch (cell.getCellType()) {
		        case Cell.CELL_TYPE_BOOLEAN:
		            ret = String.valueOf(cell.getBooleanCellValue());
		            break;
		        case Cell.CELL_TYPE_NUMERIC:
		            ret = String.valueOf(cell.getNumericCellValue());
		            break;
		        case Cell.CELL_TYPE_STRING:
		            ret = cell.getStringCellValue();
		            break;
		        case Cell.CELL_TYPE_BLANK:
		            break;
		        case Cell.CELL_TYPE_ERROR:
		            ret = "Error cell, please recheck " + cell_reference + " on Microsoft Excel";
		            break;

		        // CELL_TYPE_FORMULA will never occur
		        case Cell.CELL_TYPE_FORMULA: 
		            break;
		    }
		}
		return ret;
	}
	
//	Test code, do not remove comment mark
//	public static void main(String[] args) throws InterruptedException{
//		parseFile("Configuration.xlsx");
//		System.out.println("Result =" + getCellValueAsString("Sheet1", "C3"));
//		Thread.sleep(5000);
//	}
	
	private Workbook wb; 
	private Sheet sheet;
}
