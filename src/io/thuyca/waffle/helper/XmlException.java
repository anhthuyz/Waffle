package io.thuyca.waffle.helper;

public class XmlException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public XmlException() {
	}

	public XmlException(String message) {
		super(message);
	}

	public XmlException(Throwable cause) {
		super(cause);
	}

	public XmlException(String message, Throwable cause) {
		super(message, cause);
	}

}
