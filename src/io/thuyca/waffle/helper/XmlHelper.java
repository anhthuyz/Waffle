package io.thuyca.waffle.helper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

public class XmlHelper

{
	public static synchronized XmlHelper getXml() {
		if (xml == null) {
			xml = new XmlHelper();
		}
		return xml;
	}

	public void parseXml(String xml) throws XmlException {
		try {
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			InputStream inStream = new ByteArrayInputStream(
					xml.getBytes("UTF-8"));
			document = builder.parse(inStream);
		} catch (ParserConfigurationException e) {
			throw new XmlException("Can't configure the XML parser.", e);
		} catch (SAXException e) {
			throw new XmlException("Can't parse the xml string " + xml, e);
		} catch (IOException e) {
			throw new XmlException("Can't read the xml string " + xml, e);
		}
	}

	public void parseResource(String filename) throws XmlException {
		try {
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			InputStream inStream = getClass().getResourceAsStream(filename);
			if (inStream == null) {
				inStream = new FileInputStream(new File(filename));
			}
			document = builder.parse(inStream);
		} catch (ParserConfigurationException e) {
			throw new XmlException("Can't configure the XML parser.", e);
		} catch (SAXException e) {
			throw new XmlException("Can't parse the xml file " + filename, e);
		} catch (IOException e) {
			throw new XmlException("Can't read the xml file " + filename, e);

		}
	}

	public void setNodeValue(String Xpath, String value, String filename)
			throws Exception {
		// Exit with exception in case value is null
		if (value == null) {
			throw new Exception("Update value, new value to set is null");
		}

		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		InputStream inStream = getClass().getResourceAsStream(filename);
		if (inStream == null) {
			inStream = new FileInputStream(new File(filename));
		}
		document = builder.parse(inStream);
		XPathFactory xfactory = XPathFactory.newInstance();
		XPath xpathObj = xfactory.newXPath();
		Node node;

		try {
			node = (Node) xpathObj.evaluate(Xpath, document,
					XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw new RuntimeException(e);
		}

		node.setTextContent(value);

		// Save the doc back to the file
		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		Result output = new StreamResult(new File(filename));
		Source input = new DOMSource(document);

		transformer.transform(input, output);
	}

	public void addNewChildElement(String fileName, String parentXpath,
			String element, String value, String attribute, String attribValue)
			throws ParserConfigurationException, SAXException, IOException,
			XPathExpressionException, TransformerFactoryConfigurationError,
			TransformerException {
		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		document = builder.parse(fileName);
		XPathExpression expr = xPath.compile(parentXpath);
		Node parent = (Node) expr.evaluate(document, XPathConstants.NODE);

		Element child = document.createElement(element);
		child.setTextContent(value);
		if (attribute != "" || attribValue != "") {
			child.setAttribute(attribute, attribValue);
		}
		parent.appendChild(child);

		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		Result output = new StreamResult(new File(fileName));
		Source input = new DOMSource(document);
		transformer.transform(input, output);
	}

	public void removeAllChildren(String fileName, String parentXpath)
			throws ParserConfigurationException, SAXException, IOException,
			XPathExpressionException, TransformerFactoryConfigurationError,
			TransformerException {

		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		document = builder.parse(fileName);
		XPathExpression expr = xPath.compile(parentXpath);
		Node parent = (Node) expr.evaluate(document, XPathConstants.NODE);

		while (parent.hasChildNodes()) {
			parent.removeChild(parent.getFirstChild());
		}

		Transformer transformer = TransformerFactory.newInstance()
				.newTransformer();
		Result output = new StreamResult(new File(fileName));
		Source input = new DOMSource(document);
		transformer.transform(input, output);

	}

	public String getText(Element root, String path) throws XmlException {
		String text;
		try {
			Object eval = xPath.evaluate(path, root, XPathConstants.NODE);
			Element element = Element.class.cast(eval);
			if (element == null) {
				throw new XmlException("XPath can't find an element: " + path);
			}
			text = element.getTextContent();
		} catch (XPathExpressionException e) {
			throw new XmlException("Can't evaluate the xpath " + path, e);
		}
		return text;
	}

	public String getText(String path) throws XmlException {
		return getText(document.getDocumentElement(), path);
	}

	public Integer getInteger(Element root, String path) throws XmlException {
		Integer value = null;
		try {
			value = new Integer(Integer.parseInt(getText(root, path)));
		} catch (NumberFormatException e) {
			throw new XmlException("Element was expecting integer content "
					+ path, e);
		}
		return value;
	}

	public Integer getInteger(String path) throws XmlException {
		return getInteger(document.getDocumentElement(), path);
	}

	public boolean hasElement(Element root, String path) {
		Element element = null;
		try {
			Object eval = xPath.evaluate(path, root, XPathConstants.NODE);
			element = Element.class.cast(eval);
		} catch (XPathExpressionException e) {
			return false;
		}
		return element != null;
	}

	public boolean hasElement(String path) {
		return hasElement(document.getDocumentElement(), path);
	}

	public Element getElement(Element root, String path) throws XmlException {
		if (document == null) {
			throw new XmlException(
					"There is no document available. Did you parse one?");
		}

		Object eval;
		Node node;

		try {
			eval = xPath.evaluate(path, root, XPathConstants.NODE);
			node = Node.class.cast(eval);
		} catch (XPathExpressionException e) {
			throw new XmlException("Can't evaluate the xpath " + path, e);
		}

		return Element.class.cast(node);

	}

	public Element getElement(String path) throws XmlException {
		return getElement(document.getDocumentElement(), path);
	}

	public List<Element> getElements(Element root, String path)
			throws XmlException {
		if (document == null) {
			throw new XmlException(
					"There is no document available. Did you parse one?");
		}

		Object eval;
		NodeList nodes;

		try {
			eval = xPath.evaluate(path, root, XPathConstants.NODESET);
			nodes = NodeList.class.cast(eval);
		} catch (XPathExpressionException e) {
			throw new XmlException("Can't evaluate the xpath " + path, e);
		}

		List<Element> elements = new ArrayList<Element>();
		for (int index = 0; index < nodes.getLength(); index++) {
			elements.add(Element.class.cast(nodes.item(index)));
		}

		return elements;
	}

	public List<Element> getElements(String path) throws XmlException {
		return getElements(document.getDocumentElement(), path);
	}

	public String getStringAttribute(String path, String name)
			throws XmlException {
		return getElements(path).get(0).getAttribute(name);
	}

	public String getStringAttribute(Element element, String name) {
		return element.getAttribute(name);
	}

	public Integer getIntegerAttribute(Element element, String name)
			throws XmlException {
		Integer value = null;
		try {
			value = new Integer(Integer.parseInt(getStringAttribute(element,
					name)));
		} catch (NumberFormatException e) {
			throw new XmlException("Attribute was expecting an integer for "
					+ name, e);
		}
		return value;
	}

	public Boolean getBooleanAttribute(Element element, String name)
			throws XmlException {
		return Boolean.parseBoolean(getStringAttribute(element, name));
	}

	public String prettyPrint() throws XmlException {
		return prettyPrint(document);
	}

	public String prettyPrint(Document document) throws XmlException {
		StringWriter stringWriter = new StringWriter();

		try {
			DOMImplementation domImplementation = document.getImplementation();
			DOMImplementationLS domImplementationLS = (DOMImplementationLS) domImplementation
					.getFeature("LS", "3.0");
			LSSerializer lsSerializer = domImplementationLS
					.createLSSerializer();

			lsSerializer.getDomConfig().setParameter("format-pretty-print",
					Boolean.TRUE);
			LSOutput lsOutput = domImplementationLS.createLSOutput();
			lsOutput.setEncoding("UTF-8");
			lsOutput.setCharacterStream(stringWriter);
			lsSerializer.write(document, lsOutput);
		} catch (Exception e) {
			throw new XmlException("Failed to print the given document.", e);
		}

		return stringWriter.toString();
	}

	public List<String> getAllDirectChildrenOfAnElement(String fileName,
			String Xpath) throws Exception {
		List<String> retList = new ArrayList<String>();

		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		document = builder.parse(fileName);

		XPathExpression expr = xPath.compile(Xpath);
		Node root = (Node) expr.evaluate(document, XPathConstants.NODE);
		NodeList children = root.getChildNodes();

		for (int i = 0; i < children.getLength(); i++) {
			if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
				retList.add(children.item(i).getNodeName());
			}
		}
		return retList;
	}
	
	public List<String> getAllLeavesOfAnElement(String fileName, String Xpath) throws Exception {
		List<String> retList = new ArrayList<String>();

		DocumentBuilder builder = builderFactory.newDocumentBuilder();
		document = builder.parse(fileName);

		XPathExpression expr = xPath.compile(Xpath);
		Node root = (Node) expr.evaluate(document, XPathConstants.NODE);
		NodeList children = root.getChildNodes();

		for (int i = 0; i < children.getLength(); i++) {
			if (children.item(i).getNodeType() == Node.ELEMENT_NODE) {
				retList.add(children.item(i).getTextContent());
			}
		}
		return retList;
	}
	
	public XmlHelper() {
	}

	private static XmlHelper xml;

	private Document document = null;
	private final DocumentBuilderFactory builderFactory = DocumentBuilderFactory
			.newInstance();
	private final XPath xPath = XPathFactory.newInstance().newXPath();
}
