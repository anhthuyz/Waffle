package io.thuyca.waffle.helper;

import java.util.Random;

public class RandomHelper {
	public static synchronized RandomHelper getRandom() {
		if (rand == null) {
			rand = new RandomHelper();
		}
		return rand;
	}

	public int getRandomInt() {
		Random rand = new Random();
		return rand.nextInt();
	}
	
	
	public RandomHelper() {
	};

	private static RandomHelper rand;
}
