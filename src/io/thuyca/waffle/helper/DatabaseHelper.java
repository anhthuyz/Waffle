package io.thuyca.waffle.helper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseHelper {

	public static synchronized DatabaseHelper getDatabase() {
		if (database == null) {
			database = new DatabaseHelper();
		}
		return database;
	}

	/*
	 * Execute a query then get the result set
	 * 
	 * @param databaseDriver
	 * 
	 * @param userName
	 * 
	 * @param password
	 * 
	 * @param query
	 */
	public ResultSet executeQuery(String databaseDriver, String databaseLink,
			String userName, String password, String query) {
		Connection conn = null;
		ResultSet rs = null;

		try {
			Class.forName(databaseDriver);
			conn = DriverManager
					.getConnection(databaseLink, userName, password);
			PreparedStatement st = conn.prepareStatement(query);
			rs = st.executeQuery();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return rs;
	}

	/*
	 * Execute an update query
	 * 
	 * @param databaseDriver
	 * 
	 * @param userName
	 * 
	 * @param password
	 * 
	 * @param query
	 */
	public void executeNonQuery(String databaseDriver, String userName,
			String password, String query) {
		Connection conn = null;

		try {
			Class.forName(databaseDriver);
			conn = DriverManager.getConnection(databaseDriver, userName,
					password);
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	// public ResultSet executeListBuilderQuery
	public DatabaseHelper() {
	};

	private static DatabaseHelper database;
}
