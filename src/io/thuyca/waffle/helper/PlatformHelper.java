package io.thuyca.waffle.helper;

public class PlatformHelper {
	public static synchronized PlatformHelper getPlatform() {
		if (platformProvider == null) {
			platformProvider = new PlatformHelper();
		}
		return platformProvider;
	}

	private String getOsName() {
		String os = System.getProperty("os.name");
		return os;
	}

	public boolean isWindows() {
		return getOsName().startsWith("Windows");
	}

	public boolean isMac() {
		return getOsName().contains("Mac");
	}

	public boolean isUnix() {
		return (getOsName().contains("nux") || getOsName().contains("nix") || getOsName()
				.contains("aix"));
	}

	public PlatformHelper() {
	};

	private static PlatformHelper platformProvider;
}
