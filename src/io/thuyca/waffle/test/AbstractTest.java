package io.thuyca.waffle.test;

import org.testng.AssertJUnit;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import io.thuyca.waffle.testng.VerificationFailures;

/**HISTORY
 */

public abstract class AbstractTest
{
	protected void takeScreenshot(String path){
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    protected boolean verifyTrue(boolean condition, boolean halt)
    {
        boolean pass = true;
        if ( halt == false ) {
            try {
                AssertJUnit.assertTrue(condition);
            } catch (Throwable e) {
                pass = false;
                VerificationFailures.getFailures().addFailureForTest(Reporter.getCurrentTestResult(), e);
            }
        } else {
            AssertJUnit.assertTrue(condition);
        }
        return pass;
    }

    protected boolean verifyTrue(boolean condition)
    {
        return verifyTrue(condition, false);
    }
    
    protected boolean verifyFalse(boolean condition, boolean halt)
    {
        boolean pass = true;
        if ( halt == false ) {
            try {
                AssertJUnit.assertFalse(condition);
            } catch (Throwable e) {
                pass = false;
                VerificationFailures.getFailures().addFailureForTest(Reporter.getCurrentTestResult(), e);
            }
        } else {
            AssertJUnit.assertFalse(condition);
        }
        return pass;
        
    }
    
    protected boolean verifyFalse(boolean condition)
    {
        return verifyFalse(condition, false);
    }
    
    protected boolean verifyEquals(Object actual, Object expected, boolean halt)
    {
        boolean pass = true;
        if ( halt == false ) {
            try {
                AssertJUnit.assertEquals(actual, expected);
            } catch (Throwable e) {
                pass = false;
                VerificationFailures.getFailures().addFailureForTest(Reporter.getCurrentTestResult(), e);
            }
        } else {
            AssertJUnit.assertEquals(actual, expected);
        }
        return pass;
    }
    
    protected boolean verifyEquals(Object actual, Object expected)
    {
        return verifyEquals(actual, expected, false);
    }
    
    public AbstractTest()
    {
    	log = LogFactory.getLog(getClass());
    }
    
    protected final Log log;
    WebDriver driver = null;
}
