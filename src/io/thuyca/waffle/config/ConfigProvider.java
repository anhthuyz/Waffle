package io.thuyca.waffle.config;

import org.testng.Reporter;

import io.thuyca.waffle.helper.ExcelHelper;
import io.thuyca.waffle.helper.XmlException;
import io.thuyca.waffle.helper.XmlHelper;

public class ConfigProvider {

	// *****************************************
	// Common Methods
	//Singleton constructor
	public static synchronized ConfigProvider getProvider() {
		if (provider == null) {
			provider = new ConfigProvider();
		}
		return provider;
	}

	// *****************************************
	// Test Environment Settings Section

	public String getTestBrowser() {
		return eh.getCellValueAsString(CONFIG_SHEET, TEST_BROWSER_CELL);
	}

	
	public String getDefaultUrl() {
		return eh.getCellValueAsString(CONFIG_SHEET, DEFAULT_URL_CELL);
	}

	public int getSeekTimeout() {
		return Integer.parseInt(eh.getCellValueAsString(CONFIG_SHEET, TIMEOUT_CELL).replace(".0", ""));
	}

	// AUT Setting
	public String getUrl() {
		return eh.getCellValueAsString(CONFIG_SHEET, APP_URL_CELL);
	}
	
	public String getDriverLocation() {
		return eh.getCellValueAsString(CONFIG_SHEET, DRIVER_LOCATION_CELL);
	}
	
	// Singleton constructor
	public ConfigProvider() {
		eh = new ExcelHelper(DEFAULT_CONFIG_FILENAME);
	}
	private static ConfigProvider provider;


	// *****************************************
	// Configuration constants
	private ExcelHelper eh;
	private static final String CONFIG_SHEET = "Sheet1"; 
	private static final String DEFAULT_CONFIG_FILENAME = "Configuration.xlsx";

	// Test Environment Constants
	private static final String TEST_BROWSER_CELL = "C3";
	private static final String DEFAULT_URL_CELL = "C4";
	private static final String TIMEOUT_CELL = "C5";
	private static final String DRIVER_LOCATION_CELL = "C6";

	//  Constants
	private static final String APP_URL_CELL = "C9";
}