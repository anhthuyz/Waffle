package io.thuyca.waffle.demoqa.test;

import org.apache.log4j.PropertyConfigurator;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import io.thuyca.waffle.demoqa.page.AboutPage;
import io.thuyca.waffle.demoqa.page.HomePage;
import io.thuyca.waffle.selenium.Browser;
import io.thuyca.waffle.test.AbstractTest;

@Listeners({ ATUReportsListener.class, ConfigurationListener.class,
	MethodListener.class })

public class Draft extends AbstractTest {
	{
		System.setProperty("atu.reporter.config", "atu.properties");
	}
	
	@Test(groups = { "regression" }, description = "Demo framework test case")
	public void DEMO_TC() throws InterruptedException {
		Reporter.log("Verify About Us page is opened when user clicks on About us link");
		// Step 1: Open AUT
		homePage.waitUntilReady();
		aboutPage = homePage.goToAboutUs();
		aboutPage.waitUntilReady();
		
		// VP: Verify that About Us page is loaded
		boolean result = verifyTrue(aboutPage.isLoaded());
		if (result){
			Reporter.log("PASSED: About us page is loaded when user clicks on About us link");
		} else {
			Reporter.log("FAILED: About us page is not loaded when user clicks on About us link");
		}
		
	}
	

	@BeforeMethod
	public void setUp() {
		String log4jConfPath = "log4j.properties";
		PropertyConfigurator.configure(log4jConfPath);
		Browser.getBrowser().launch();
		Reporter.log("Go to DemoQA page");
		Browser.getBrowser().goToAut();
		aboutPage = Browser.getBrowser().getAboutPage();
		homePage = Browser.getBrowser().getHomePage();
		ATUReports.setWebDriver(Browser.getBrowser().getDriver());
	}

	@AfterMethod
	public void cleanUp() {
		Browser.getBrowser().goHome();
		Browser.getBrowser().shutdown();
	}

	private HomePage homePage = null;
	private AboutPage aboutPage = null;
}
