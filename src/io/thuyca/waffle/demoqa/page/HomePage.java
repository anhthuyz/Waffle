package io.thuyca.waffle.demoqa.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.thuyca.waffle.config.ConfigProvider;
import io.thuyca.waffle.selenium.AbstractPage;
import io.thuyca.waffle.selenium.Browser;
import io.thuyca.waffle.test.Waiter;

public class HomePage extends AbstractPage {

	@Override
	protected String getLoadedLocator() {
		return "//a[@id='log_out']";
	}

	public void waitUntilReady() {
		Waiter.getWaiter().waitForElement(By.xpath(lblAbout), timeOutInSeconds);
	}
	
	public boolean isDisplayed(){
		return driver.findElement(By.xpath(lnkAbout)) != null;
	}

	public AboutPage goToAboutUs(){
		WebElement userMenu = driver.findElement(By.xpath(lnkAbout));
		userMenu.click();
		return Browser.getBrowser().getAboutPage();
	}
	
	
	// Page XPath Locators
	private String lnkAbout = "//a[contains(text(),'About us')]";
	private String lblAbout = "//h3[contains(text(),'About Us')]";
	
	// Test data
	private int timeOutInSeconds = ConfigProvider.getProvider()
			.getSeekTimeout();	
}