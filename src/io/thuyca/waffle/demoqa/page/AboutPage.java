package io.thuyca.waffle.demoqa.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import io.thuyca.waffle.config.ConfigProvider;
import io.thuyca.waffle.selenium.AbstractPage;
import io.thuyca.waffle.selenium.Browser;
import io.thuyca.waffle.test.Waiter;

public class AboutPage extends AbstractPage {

	@Override
	protected String getLoadedLocator() {
		// TODO Auto-generated method stub
		return "//h1[contains(text(),'About us')]";
	}
	
	public void waitUntilReady(){
		Waiter.getWaiter().waitForElement(By.xpath(lblAbout), timeOutInSeconds);
	}
	
	public boolean isLoaded(){
		return driver.findElement(By.xpath(lblAbout))!=null;
	}
	
	// Interface XPath Locators
	private String lblAbout = "//h1[contains(text(),'About us')]";  
	
	// Test data
	private int timeOutInSeconds = ConfigProvider.getProvider()
			.getSeekTimeout();	
}




