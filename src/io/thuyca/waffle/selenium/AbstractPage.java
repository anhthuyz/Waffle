package io.thuyca.waffle.selenium;

import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.thuyca.waffle.selenium.Browser;

public abstract class AbstractPage {
	public boolean isDisplayed(String xpathString) {
		final boolean isPresent = driver.findElements(By.xpath(xpathString))
				.size() > 0;
		if (isPresent == false) {
			log.debug("Login page identifier " + xpathString
					+ " was not found.");
		}
		return isPresent;
	}

	public void refresh() {
		driver.navigate().refresh();

	}

	public void back() {
		driver.navigate().back();
	}

	public String getPageTitle(WebDriver driver) {
		return driver.getTitle();
	}

	/**
	 * Check whether or not an option item is in the select list
	 */
	public boolean isOptionInSelectList(final String locator, final String key) {
		boolean found = false;
		Select select = new Select(driver.findElement(By.xpath(locator)));
		for (WebElement option : select.getOptions()) {
			if (option.getText().equals(key) == true) {
				found = true;
				break;
			}
		}
		return found;
	}
	
	public void scrollToTop() {
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("scroll(250, 0)"); 
	}
	
	public void scrollToBottom(){
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollTo(0,Math.max(document.documentElement.scrollHeight,document.body.scrollHeight,document.documentElement.clientHeight));");
	}
	
	public boolean isTextPresentOnTextBoxMatch(By by, String arg) {
		WebElement textbox = driver.findElement(by);
		String txtValue = textbox.getAttribute("value");
		return txtValue.equals(arg); 
	}
	
	/*
	 * Work around solution for clicking some wrapped hyperlink
	 * 
	 * @param By
	 * 			Locator to identify element
	 */
	public void safeClick(By by) {
		WebElement element = driver.findElement(by);
		Actions builder = new Actions(driver);
		Actions sClick = builder.moveToElement(element).click();
		sClick.build().perform();
	}
		
	public String getSelectedLabel(final String locator) {
		String label = null;
		WebElement select = driver.findElement(By.xpath(locator));
		for (WebElement option : select.findElements(By.tagName("option"))) {
			if (option.isSelected() == true) {
				label = option.getText();
			}
		}
		return label;
	}

	protected String createLocator(String... parts) {
		StringBuilder locator = new StringBuilder();
		for (String part : parts) {
			locator.append(part);
		}
		log.debug("Locator created: " + locator);
		return locator.toString();
	}

	/**
	 * Check control exist
	 * 
	 * @param controlName
	 * @return boolean
	 */

	public String getCurrentURL() {
		return driver.getCurrentUrl();
	}

	/**
	 * Check a control is link or not.
	 * 
	 * @return is control a link
	 * 
	 */
	public boolean isControlLink(String linkText) {
		boolean strResult;
		WebElement element = driver.findElement(By.linkText(linkText));

		if (element.getAttribute("href") != null) {
			strResult = true;
		} else {
			strResult = false;
		}
		return strResult;
	}

	/**
	 * Check if an element is displayed or not
	 * 
	 * @return is a boolean state
	 * 
	 */
	public boolean isElementPresent(String Xpath) {
		boolean present;
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		present = driver.findElements(By.xpath(Xpath)).size() != 0;
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		return present;
	}

	public void typeTextOnControl(String text, WebElement element) {
		element.sendKeys(text);
	}

	/**
	 * Check if an text box have max size limit
	 * 
	 * @param By
	 *            locator to identify element @ param testData string to enter
	 *            into text box under test
	 * @param maxSize
	 *            Max text box size limit
	 * @return true if passed, false if failed
	 * 
	 */
	public boolean maxTextBoxSizeLimit(final By by, String testData,
			long maxSize) {
		WebElement element = driver.findElement(by);
		typeTextOnControl(testData, element);
		String tmp = element.getAttribute("value");
		return tmp.length() == maxSize;
	}

	public void selectComboBoxItemByValue(final By by, String selectValue) {
		Select selectObj = new Select(driver.findElement(by));
		selectObj.selectByValue(selectValue);
	}
	
	public String getComboBoxSelectedValue(final By by) {
		Select selectObj = new Select(driver.findElement(by));
		WebElement option = selectObj.getFirstSelectedOption();
		return option.getText();
	}
	
	protected AbstractPage() {
		driver = Browser.getBrowser().getDriver();
		log = LogFactory.getLog(getClass());
		log.debug("Created page abstraction for " + getClass().getName());
	}

	protected WebDriver driver;

	protected abstract String getLoadedLocator();

	protected final Log log;
}
