package io.thuyca.waffle.selenium;

/* ****************************************************
 * This class is the central class that hold all page
 * instance among pages during test process. 
 * Created by: Thuy Cao
 * */

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Reporter;

import io.thuyca.waffle.config.ConfigProvider;
import io.thuyca.waffle.demoqa.page.*;

public class Browser {
	public static synchronized Browser getBrowser() {
		if (instance == null) {
			instance = new Browser();
		}
		return instance;
	}

	public HomePage getHomePage() {
		if (homePage == null) {
			homePage = new HomePage();
		}
		return homePage;
	}

	public AboutPage getAboutPage() {
		if (about == null) {
			about = new AboutPage();
		}
		return about;
	}

	public WebDriver getDriver() {
		return driver;
	}

	@Deprecated
	public void launch(final String driverClass, final String url) {

		try {
			Class<?> clazz;
			clazz = Class.forName(driverClass);
			Class<?>[] constructorTypes = new Class[] {};
			Object[] constructorArgs = new Object[] {};
			Constructor<?> constructor = clazz.getConstructor(constructorTypes);
			driver = WebDriver.class.cast(constructor
					.newInstance(constructorArgs));
			driver.get(url);
			driver.manage().window().maximize();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void launch() {
		String defaultUrl = ConfigProvider.getProvider().getDefaultUrl();
		String browser = ConfigProvider.getProvider().getTestBrowser();
		int loadTimeOut = ConfigProvider.getProvider().getSeekTimeout();
		System.setProperty("webdriver.chrome.driver", ConfigProvider.getProvider().getDriverLocation());
		if (browser.equalsIgnoreCase("ie")) {
			driver = new InternetExplorerDriver();
		} else if (browser.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("chrome")) {
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("safari")) {
			driver = new SafariDriver();
		}
		
		driver.get(defaultUrl);
		driver.manage().window().maximize();
		driver.manage().timeouts()
				.pageLoadTimeout(loadTimeOut, TimeUnit.SECONDS);
	}

	public void takeScreenshot(String name) {
		TakesScreenshot view = TakesScreenshot.class.cast(driver);
		File screenshot = view.getScreenshotAs(OutputType.FILE);
		File destination = new File(name + ".png");
		try {
			FileUtils.copyFile(screenshot, destination);
			log.info("Screenshot saved to " + destination.getAbsolutePath());
		} catch (IOException e) {
			log.error(
					"Failed to write screenshot to "
							+ destination.getAbsolutePath(), e);
		}
	}

	public void goToAut() {

		String url;
			url = ConfigProvider.getProvider().getUrl();
			open(url);
	}

	public void goHome() {
		open(homeUrl);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void open(String url) {
		driver.get(url);
	}

	public void rememberLocation() {
		rememberedUrl = driver.getCurrentUrl();
	}

	public void recallLocation() {
		if (rememberedUrl != null) {
			driver.get(rememberedUrl);
		}
	}

	public void stopTest() {
		driver.quit();
	}

	public void shutdown() {
		try {
			driver.quit();
			driver = null;
		} catch (NullPointerException e) {
			Reporter.log("Aborted by user");
			throw e;
		}
	}
	/**
	 * Some browser actions may open a new window. This method will push the
	 * current handle onto a stack, discover the new handle, then switch to the
	 * new window. This will only work if we open one window at a time, operate
	 * on the new window, then close it.
	 */
	public void registerNewWindow() {
		openWindowHandles.push(driver.getWindowHandle());

		// Find the new handle by the intersection of the open window handles
		// and the entire set.
		Set<String> newHandles = driver.getWindowHandles();
		newHandles.removeAll(openWindowHandles);
		if (newHandles.size() != 1) {
			log.error("The register new window stack is out of sync.");
		}
		String handle = newHandles.iterator().next();

		driver.switchTo().window(handle);
	}

	public void closeNewWindow() {
		if (openWindowHandles.size() < 1) {
			log.error("Called close new window when only the main browser was open.");
		} else {
			driver.close();
			driver.switchTo().window(openWindowHandles.pop());
		}
	}

	static String getBrowserFromConfig() {
		String retVal = "";
		ConfigProvider conf = new ConfigProvider();
		retVal = conf.getTestBrowser();
		return retVal;
	}

	private static Browser instance = null;

	// Page objects initialization
	private HomePage homePage = null;
	private AboutPage about = null;

	// Common Initialization
	private WebDriver driver = null;
	private String homeUrl = "about:blank";
	private String rememberedUrl = null;

	private final Stack<String> openWindowHandles = new Stack<String>();
	private static final Log log = LogFactory.getLog(Browser.class);

}
